package by.epam.training.beans;

public class Page {
    private static int pageCounter;
    private int numberPage;
    private String parentBook;
    private String dataPage;

    public Page() {
    }

    public Page(String parentBook, String dataPage) {
        this.parentBook = parentBook;
        this.numberPage = ++pageCounter;
        this.dataPage = dataPage;
    }

    public static int getPageCounter() {
        return pageCounter;
    }

    public static void setPageCounter(int pageCounter) {
        Page.pageCounter = pageCounter;
    }

    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }

    public void setNumberPage() {
        this.numberPage = ++pageCounter;
    }

    public String getParentBook() {
        return parentBook;
    }

    public void setParentBook(String parentBook) {
        this.parentBook = parentBook;
    }

    public String getDataPage() {
        return dataPage;
    }

    public void setDataPage(String dataPage) {
        this.dataPage = dataPage;
    }

    @Override
    public String toString() {
        return dataPage;
    }
}
