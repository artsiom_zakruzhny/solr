package by.epam.training.beans;

import java.util.Date;
import java.util.List;

public class Book {
    private String author;
    private String authorFacet;
    private String publisher;
    private String isdn;
    private String creationDate;
    private String publicationDate;
    private String uploadDate;
    private String title;
    private String annotation;
    private List<Page> contents;
    private String coverImage;

    public Book() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorFacet() {
        return authorFacet;
    }

    public void setAuthorFacet(String authorFacet) {
        this.authorFacet = authorFacet;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public List<Page> getContents() {
        return contents;
    }

    public void setContents(List<Page> contents) {
        this.contents = contents;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    @Override
    public String toString() {
        return author + "; " + publisher + "; " + isdn + "; " + creationDate +
                "; " + publicationDate + "; " + uploadDate + "; " + title +
                "; " + annotation + "; " + contents + "; " + coverImage;
    }
}
