package by.epam.training.parsers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import by.epam.training.beans.Book;
import by.epam.training.beans.Page;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static by.epam.training.utilities.Constants.*;

public class XpathParserBook {

    private FileInputStream fileInputStream;

    public XpathParserBook(FileInputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public Book parse() throws ParserConfigurationException, SAXException, XPathExpressionException {
        Document xmlDocument;
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            xmlDocument = builder.parse(fileInputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException("fail with file...", e);
        }

        XPath xPath = XPathFactory.newInstance().newXPath();
        Node authorFirstNameNode = (Node) xPath.compile(AUTHOR_FIRSTNAME).evaluate(xmlDocument, XPathConstants.NODE);
        Node authorLastNameNode = (Node) xPath.compile(AUTHOR_LASTNAME).evaluate(xmlDocument, XPathConstants.NODE);
        Node publisherNode = (Node) xPath.compile(PUBLISHER).evaluate(xmlDocument, XPathConstants.NODE);
        Node isdnNode = (Node) xPath.compile(ISDN).evaluate(xmlDocument, XPathConstants.NODE);
        Node creationDateNode = (Node) xPath.compile(CREATION_DATE).evaluate(xmlDocument, XPathConstants.NODE);
        Node publicationDateNode = (Node) xPath.compile(PUBLICATION_DATE).evaluate(xmlDocument, XPathConstants.NODE);
        Node titleNode = (Node) xPath.compile(TITLE).evaluate(xmlDocument, XPathConstants.NODE);
        NodeList annotationNodeSet = (NodeList) xPath.compile(ANNOTATION).evaluate(xmlDocument, XPathConstants.NODESET);
        NodeList contentsNodeSet = (NodeList) xPath.compile(CONTENTS).evaluate(xmlDocument, XPathConstants.NODESET);
        Node coverNode = (Node) xPath.compile(COVER).evaluate(xmlDocument, XPathConstants.NODE);

        Book book = new Book();
        if (authorFirstNameNode != null && authorLastNameNode != null) {
            book.setAuthor(authorFirstNameNode.getTextContent()+" "+authorLastNameNode.getTextContent());
        }
        if (publicationDateNode != null) {
            book.setPublisher(publisherNode.getTextContent());
        }
        if (isdnNode != null) {
            book.setIsdn(isdnNode.getTextContent());
        }
        if (creationDateNode != null) {
            book.setCreationDate(creationDateNode.getTextContent());
        }
        if (publicationDateNode != null) {
            book.setPublicationDate(publicationDateNode.getTextContent());
        }
        if (titleNode != null) {
            book.setTitle(titleNode.getTextContent());
        }
        if (annotationNodeSet != null) {
            book.setAnnotation(getStrAnnotation(annotationNodeSet));
        }
        if (contentsNodeSet != null) {
            book.setContents(getPages(titleNode.getTextContent(), contentsNodeSet));
        }
        if (coverNode != null) {
            book.setCoverImage(coverNode.getTextContent());
        }
        System.out.println(book.getTitle());
        return book;
    }

    private List<Page> getPages(String parentBook, NodeList contentsNodeSet) {
        Page.setPageCounter(0);
        final int SYMB_AMOUNT_ON_PAGE = 600;
        List<Page> pages = new ArrayList<>();
        for (int i = 0; i < contentsNodeSet.getLength(); i++) {
            StringBuilder stringBuilder = new StringBuilder();
            NodeList childNodes = contentsNodeSet.item(i).getChildNodes();
            for (int j = 0; j < childNodes.getLength(); j++) {
                Node node = childNodes.item(j);
                if (node.getNodeName().equals("p")) {
                    stringBuilder.append(node.getTextContent());
                }
            }
            int range = 0;
            for (int k = 0; k < stringBuilder.length() / SYMB_AMOUNT_ON_PAGE; k++) {
                Page page = new Page(parentBook, stringBuilder.substring(range, range += SYMB_AMOUNT_ON_PAGE));
                pages.add(page);
            }
            Page page = new Page(parentBook, stringBuilder.substring(range, range + stringBuilder.length() % SYMB_AMOUNT_ON_PAGE));
            pages.add(page);
        }
        return pages;
    }


    private String getStrAnnotation(NodeList annotationNodeSet) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < annotationNodeSet.getLength(); i++) {
            stringBuilder.append(annotationNodeSet.item(i).getTextContent()).append(" ");
        }
        return stringBuilder.toString();
    }
}
