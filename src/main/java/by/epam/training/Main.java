package by.epam.training;

import by.epam.training.beans.Book;
import by.epam.training.parsers.XpathParserBook;
import by.epam.training.solr.client.ClientSolrJ;
import org.apache.solr.client.solrj.SolrServerException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        File folder = new File("D:/solr-7.2.0/example/library/");
        File[] folderEntries = folder.listFiles();
        ClientSolrJ solrJ = new ClientSolrJ();
        if (folderEntries != null) {
            int counter = 1;
            for (File file : folderEntries) {
                System.out.println(file);
                XpathParserBook parser;
                try {
                    parser = new XpathParserBook(new FileInputStream(file));
                    try {
                        System.out.print("book " + counter+" ");
                        Book book = parser.parse();
                        solrJ.index(book);
                        counter++;
                    } catch (ParserConfigurationException | SAXException | SolrServerException | XPathExpressionException | IOException e) {
                        throw new RuntimeException("FAIL WHILE INDEX DOCUMENT...", e);
                    }
                } catch (FileNotFoundException e) {
                    throw new IllegalArgumentException("File not found...", e);
                }
            }
        }
    }
}
