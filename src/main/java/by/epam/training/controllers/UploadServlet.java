package by.epam.training.controllers;

import by.epam.training.beans.Book;
import by.epam.training.parsers.XpathParserBook;
import by.epam.training.solr.client.ClientSolrJ;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.solr.client.solrj.SolrServerException;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class UploadServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/uploadBooks.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClientSolrJ solrJ = new ClientSolrJ();
        List<FileItem> items = getUploadFiles(req);
        int i=1;
        for (FileItem item : items) {
            Book book = getParsingBook(item);
            indexBook(solrJ, book);
            System.out.print("book "+i);
            i++;
        }
        solrJ.closeClient();
        resp.sendRedirect("library");
    }

    private static Book getParsingBook(FileItem item) {
        XpathParserBook parser;
        try {
            parser = new XpathParserBook((FileInputStream) item.getInputStream());
            return parser.parse();
        } catch (ParserConfigurationException | SAXException | XPathExpressionException e) {
            throw new IllegalArgumentException("Fail while parsing file...", e);
        } catch (IOException e) {
            throw new RuntimeException("Fail with file...", e);
        }
    }

    private static List<FileItem> getUploadFiles(HttpServletRequest req) throws ServletException {
        try {
            return new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);
        } catch (FileUploadException e) {
            throw new ServletException("Cannot parse request.", e);
        }
    }

    private static void indexBook(ClientSolrJ solrJ, Book book) {
        try {
            solrJ.index(book);
        } catch (IOException e) {
            throw new IllegalArgumentException("Fail with file...", e);
        } catch (SolrServerException e) {
            throw new RuntimeException("Fail with solr...", e);
        }
    }
}
