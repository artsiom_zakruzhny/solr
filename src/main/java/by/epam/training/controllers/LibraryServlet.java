package by.epam.training.controllers;

import by.epam.training.solr.client.ClientSolrJ;
import org.apache.solr.client.solrj.SolrServerException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LibraryServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        ClientSolrJ solrJ = new ClientSolrJ();
        try {
            solrJ.getSuggest("0", "authorSuggester");
            solrJ.getSuggest("0", "titleSuggester");
            solrJ.getSuggest("0", "publisherSuggester");
        }catch (IOException | SolrServerException e){
            System.err.println("Timeout occured while waiting response from server...");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/index.html").forward(req, resp);
    }

}
