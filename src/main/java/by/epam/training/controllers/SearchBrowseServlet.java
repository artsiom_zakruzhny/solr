package by.epam.training.controllers;

import by.epam.training.solr.client.ClientSolrJ;
import com.google.gson.Gson;
import org.apache.solr.client.solrj.SolrServerException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SearchBrowseServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClientSolrJ clientSolrJ = new ClientSolrJ();
        String q = req.getParameter("q");
        String author = req.getParameter("author");
        String title = req.getParameter("title");
        String publisher = req.getParameter("publisher");
        String suggest = req.getParameter("suggest");
        if (req.getParameter("page") != null) {
            int page = Integer.parseInt(req.getParameter("page"));
            try {
                Gson gson = new Gson();
                String json = gson.toJson(clientSolrJ.getPage(page, q, author, title, publisher));
                resp.getWriter().print(json);
            } catch (SolrServerException e) {
                throw new RuntimeException("Fail with solr...", e);
            }
        } else {
            req.setAttribute("letter", suggest);
            req.setAttribute("text", q);
            try {
                req.setAttribute("authorSuggester", clientSolrJ.getSuggest(suggest, "authorSuggester"));
                req.setAttribute("titleSuggester", clientSolrJ.getSuggest(suggest, "titleSuggester"));
                req.setAttribute("publisherSuggester", clientSolrJ.getSuggest(suggest, "publisherSuggester"));
                req.setAttribute("books", clientSolrJ.getBooks(q, author, title, publisher));
                req.setAttribute("amount_result", clientSolrJ.getQueryFaceting(author, title, publisher));
            } catch (SolrServerException e) {
                throw new RuntimeException("Fail with solr...", e);
            }
            req.getRequestDispatcher("/search-browseBooks.jsp").forward(req, resp);
        }
    }

}
