package by.epam.training.interfaces;

import by.epam.training.beans.Book;
import by.epam.training.beans.Page;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.List;

public interface CustomSolrClient {

    void index(Book book) throws IOException, SolrServerException;

    List<Book> getBooks(String q, String author, String title, String publisher) throws IOException, SolrServerException;

    Page getPage(int pageNumber, String q, String author, String title, String publisher) throws IOException, SolrServerException;

    List<String> getSuggest(String suggestQuery, String suggestDictionary) throws IOException, SolrServerException;

    void closeClient() throws IOException;
}
