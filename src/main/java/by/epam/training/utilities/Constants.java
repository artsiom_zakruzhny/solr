package by.epam.training.utilities;

public class Constants {
    public static final String AUTHOR_FIRSTNAME = "/FictionBook/description/title-info/author/first-name";
    public static final String AUTHOR_LASTNAME = "/FictionBook/description/title-info/author/last-name";
    public static final String PUBLISHER = "/FictionBook/description/publish-info/publisher";
    public static final String ISDN = "/FictionBook/description/publish-info/isbn";
    public static final String CREATION_DATE = "/FictionBook/description/document-info/date/@value";
    public static final String PUBLICATION_DATE = "/FictionBook/description/publish-info/year";
    public static final String TITLE = "/FictionBook/description/title-info/book-title";
    public static final String ANNOTATION = "/FictionBook/description/title-info/annotation/p";
    public static final String CONTENTS = "/FictionBook/body/section";
    public static final String COVER = "/FictionBook/binary[1]";
}
