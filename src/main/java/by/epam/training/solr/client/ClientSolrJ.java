package by.epam.training.solr.client;

import by.epam.training.beans.Book;
import by.epam.training.beans.Page;
import by.epam.training.interfaces.CustomSolrClient;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SuggesterResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.*;

public class ClientSolrJ implements CustomSolrClient {

    private final static int AMOUNT_STR_NAME_PACKAGE = 29; //package by.epam.training.Page
    private final static String NAME_CORE = "library_core";
    private final SolrClient client;

    public ClientSolrJ() {
        client = getSolrClient();
    }

    private HttpSolrClient getSolrClient() {
        String solrUrl = "http://localhost:8983/solr";
        return new HttpSolrClient.Builder(solrUrl)
                .withConnectionTimeout(10000)
                .withSocketTimeout(60000)
                .build();
    }

    public void index(Book book) throws IOException, SolrServerException {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.addField("author", book.getAuthor());
        doc.addField("author_for_faceting", book.getAuthorFacet());
        doc.addField("publisher", book.getPublisher());
        doc.addField("ISDN", book.getIsdn());
        doc.addField("creation_date", book.getCreationDate());
        doc.addField("publication_date", book.getPublicationDate());
        doc.addField("upload_date", new Date());
        doc.addField("title", book.getTitle());
        doc.addField("annotation", book.getAnnotation());
        doc.addField("cover", book.getCoverImage());
        doc.addField("content_type", "parent");
        for (Page pages : book.getContents()) {
            SolrInputDocument page = new SolrInputDocument();
            page.addField("id", pages.getNumberPage());
            page.addField("parent_book", pages.getParentBook());
            page.addField("page", pages);
            page.addField("content_type", "child");
            doc.addChildDocument(page);
        }
        client.add(NAME_CORE, doc);
        client.commit(NAME_CORE);
        System.out.println("finish indexing.");
    }

    public List<Book> getBooks(String q, String author, String title, String publisher) throws IOException, SolrServerException {
        final int MAX_BOOK_AMOUNT = 5006;
        if ("".equals(q))
            q = "\"\"";
        if ("".equals(author))
            author = "\"\"";
        if ("".equals(title))
            title = "\"\"";
        if ("".equals(publisher))
            publisher = "\"\"";
        SolrQuery query = new SolrQuery(" author:" + author + " title:" + title + " publisher:" + publisher +
                "{!parent which=content_type:parent}page:" + q + "");
        if (author == null && title == null && publisher == null && q == null) {
            query = new SolrQuery("author:\"\"title:\"\"publisher:\"\"{!parent which=content_type:parent}");
            query.setRows(10);
            query.setStart(0);
        } else {
            query.setRows(MAX_BOOK_AMOUNT);
            query.setStart(0);
        }
        if (!"\"\"".equals(author) && author != null) {
            query.addFilterQuery("author:\"" + author + "\"");
            query.addFacetQuery("author:\"" + author + "\"");
        }
        if (!"\"\"".equals(title) && title != null) {
            query.addFilterQuery("title:\"" + title + "\"");
            query.addFacetQuery("title:\"" + title + "\"");
        }
        if (!"\"\"".equals(publisher) && publisher != null) {
            query.addFilterQuery("publisher:\"" + publisher + "\"");
            query.addFacetQuery("publisher:\"" + publisher + "\"");
        }
        query.addFacetField("publication_date");
        query.addHighlightField("page, author, title, publisher");
        query.setHighlight(true);
        query.setHighlightSimplePre("<strong>");
        query.setHighlightSimplePost("</strong>");
        System.out.println(query);
        QueryResponse response = client.query(NAME_CORE, query);
        return createBooksFromResult(response);
    }


    public Page getPage(int pageNumber, String q, String author, String title, String publisher) throws IOException, SolrServerException {
        Page page = new Page();
        if ("".equals(q))
            q = "\"\"";
        if ("".equals(publisher))
            publisher = "\"\"";
        SolrQuery query = new SolrQuery("page:" + q + "{!child of=content_type:parent}author:" + author +
                " title:\"" + title + "\" publisher:" + publisher);
        query.addFilterQuery("id:" + pageNumber);
        query.addFilterQuery("parent_book:" + "\"" + title + "\"");
        query.addHighlightField("page");
        query.setHighlight(true);
        query.setHighlightFragsize(0);
        query.setHighlightSnippets(100);
        query.setHighlightSimplePre("<strong>");
        query.setHighlightSimplePost("</strong>");
        System.out.println("page query = " + query);
        QueryResponse response = client.query(NAME_CORE, query);
        SolrDocumentList documents = response.getResults();
        for (SolrDocument document : documents) {
            String strPage = String.valueOf(document.getFieldValue("page"));
            System.out.println(strPage);
            strPage = strPage.substring(AMOUNT_STR_NAME_PACKAGE, strPage.length() - 1); //cut name of package
            System.out.println("*" + strPage);
            String highlight = getHighlight(response);
            page.setNumberPage(pageNumber);
            if (highlight != null) {
                page.setDataPage(highlight);
            } else {
                page.setDataPage(strPage);
            }
        }
        return page;
    }

    public List<String> getSuggest(String suggestQuery, String suggestDictionary) throws IOException, SolrServerException {
        System.out.println("start " + suggestDictionary);
        List<String> suggestions = new ArrayList<>();
        if (suggestQuery != null) {
            SolrQuery query = new SolrQuery();
            query.setRequestHandler("/suggest");
            query.set("suggest", "true");
            if ("0".equals(suggestQuery)) { // first start and build suggest
                query.set("suggest.build", "true");
            } else {
                query.set("suggest.build", "false"); // not first start
            }
            query.set("suggest.dictionary", suggestDictionary);
            query.set("suggest.q", suggestQuery);
            QueryResponse response = client.query(NAME_CORE, query);
            SuggesterResponse suggesterResponse = response.getSuggesterResponse();
            if (suggesterResponse != null) {
                Map<String, List<String>> suggestedTerms = suggesterResponse.getSuggestedTerms();
                suggestions = suggestedTerms.get(suggestDictionary);
            }
        }
        System.out.println("end " + suggestDictionary);
        return suggestions;
    }

    public void closeClient() throws IOException {
        if (client != null) {
            client.close();
        }
    }

    private static List<Book> createBooksFromResult(QueryResponse response) {
        SolrDocumentList documents = response.getResults();
        List<Book> library = new ArrayList<>();
        for (SolrDocument document : documents) {
            Book book = new Book();
            if (document.getFieldValue("author") != null) {
                String author = String.valueOf(document.getFieldValue("author"));
                author = author.substring(1, author.length() - 1); //cut on start and end []
                book.setAuthor(author);
            }
            if (document.getFieldValue("publisher") != null) {
                String publisher = String.valueOf(document.getFieldValue("publisher"));
                publisher = publisher.substring(1, publisher.length() - 1); //cut on start and end []
                book.setPublisher(publisher);
            }
            if (document.getFieldValue("title") != null) {
                String title = String.valueOf(document.getFieldValue("title"));
                title = title.substring(1, title.length() - 1).replace("'", "[quote]"); //cut on start and end []
                book.setTitle(title);
            }
            book.setIsdn(String.valueOf(document.getFieldValue("ISDN")));
            book.setCreationDate(String.valueOf(document.getFieldValue("creation_date")));
            book.setPublicationDate(String.valueOf(document.getFieldValue("publication_date")));
            book.setUploadDate(String.valueOf(document.getFieldValue("upload_date")));
            book.setAnnotation(String.valueOf(document.getFieldValue("annotation")).substring(1, String.valueOf(document.getFieldValue("annotation")).length() - 1));
            book.setCoverImage(String.valueOf(document.getFieldValue("cover")).substring(1, String.valueOf(document.getFieldValue("cover")).length() - 1));
            System.out.println(book.getTitle());
            library.add(book);
        }
        return library;
    }


    private static String getHighlight(QueryResponse response) {
        final String HIGHLIGHT_FIELD = "page";
        String highlight = null;
        Map<String, Map<String, List<String>>> hitHighlightedMap = response.getHighlighting();
        if (hitHighlightedMap != null) {
            for (Map.Entry<String, Map<String, List<String>>> entry : hitHighlightedMap.entrySet()) {
                Map<String, List<String>> map = entry.getValue();
                List<String> highlights = map.get(HIGHLIGHT_FIELD);
                if (highlights != null) {
                    for (String str : highlights) {
                        System.out.println("highlight = " + str);
                        highlight = str.substring(AMOUNT_STR_NAME_PACKAGE - 1); //cut name of package and get just text of page
                    }
                }
            }
        }
        return highlight;
    }

    public Map<String, Integer> getQueryFaceting(String author, String title, String publisher) throws IOException, SolrServerException {
        SolrQuery query = new SolrQuery("*:*");
        if (!"\"\"".equals(author) && author != null) {
            query.addFacetQuery("author:\"" + author + "\"");
        }
        if (!"\"\"".equals(title) && title != null) {
            query.addFacetQuery("title:\"" + title + "\"");
        }
        if (!"\"\"".equals(publisher) && publisher != null) {
            query.addFacetQuery("publisher:\"" + publisher + "\"");
        }
        query.addFacetField("publication_date");
        QueryResponse response = client.query(NAME_CORE, query);
        List<FacetField.Count> facetResults = response.getFacetField("publication_date").getValues();
        for (FacetField.Count count : facetResults) {
            System.out.println(count);
        }
        Map<String, Integer> facetQueryMap = response.getFacetQuery();
        return facetQueryMap;
    }
}
