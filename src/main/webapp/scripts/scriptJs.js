
function readBook(name){
    var div = document.getElementById(name);
    if (div.hidden == false) {
    	div.hidden = true;
    } else {
    	div.hidden = false;
    }
}

function getAuthorByFirstLetter(id){
    var letter = document.getElementById(id).innerHTML.substring(1);
    var xhr = new XMLHttpRequest();
        xhr.open('GET', 'search_browse?authorSuggest=' + letter, true);
        xhr.send();
    alert(letter);
}


function getFirstPage(text, book, author, title, publisher){
    title = title.replace("[quote]", "'");
    var div = document.getElementById("content"+book);
    if (div.hidden == false) {
        div.hidden = true;
    } else {
    	div.hidden = false;
    }
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'search_browse?q='+text+'&author='+author+'&title='+title+'&publisher='+publisher+'&page=' + 1, true);
    xhr.send();
    xhr.onload = function() {
        var strPage = xhr.responseText;
        var pageObj = JSON.parse(strPage);
        putPageData(book, pageObj);
    }
}

function nextPage(text, book, author, title, publisher){
    title = title.replace("[quote]", "'");
    var numberPage = document.getElementById("number_Book"+book).innerHTML;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'search_browse?q='+text+'&author='+author+'&title='+title+'&publisher='+publisher+'&page=' + (++numberPage), true);
    xhr.send();
    xhr.onload = function() {
        var strPage = xhr.responseText;
        var pageObj = JSON.parse(strPage);
        if(pageObj.numberPage == 0){
            getFirstPage();
        }else{
            putPageData(book, pageObj);
        }
    }
}

function previousPage(text, book, author, title, publisher){
    title = title.replace("[quote]", "'");
    var numberPage = document.getElementById("number_Book"+book).innerHTML;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'search_browse?q='+text+'&author='+author+'&title='+title+'&publisher='+publisher+'&page=' + (--numberPage), true);
    xhr.send();
    xhr.onload = function() {
        var strPage = xhr.responseText;
        var pageObj = JSON.parse(strPage);
        if(pageObj.numberPage == 0){
            //getFirstPage();
        }else{
            putPageData(book, pageObj);
        }
    }
}

function putPageData(book, pageObj){
    var dataPage = document.getElementById("page_Book"+book);
    var numberP = document.getElementById("number_Book"+book);
    dataPage.innerHTML = pageObj.dataPage;
    numberP.innerHTML = pageObj.numberPage;
}
