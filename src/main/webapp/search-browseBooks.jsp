<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="by.epam.training.beans.Book"%>
<%@ page import="by.epam.training.beans.Page"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="private">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,700|Raleway:200&amp;subset=cyrillic-ext"
            rel="stylesheet">
    <script src="scripts/scriptJs.js"></script>
    <title>Library</title>
</head>
<body>
<header>
    <h1>Search and Browse books...</h1>
</header>
<section>
        <form action="library">
            <div class="submit-file">
                <input type="submit" value="Go to Main page" class="submit">
            </div>
        </form>
        <div id="searchByFields">
            <form action="search_browse" method="get">
                <p><input type="search" name="author" placeholder="Author" id="author_search">
                <input type="search" name="title" placeholder="Title" id="title_search">
                <input type="search" name="publisher" placeholder="Publisher" id="publisher_search"></p>
                <p><input type="search" name="q" placeholder="text_search" id="text_search">
                <input type="submit" value="Search"></p>
            </form>
        </div>
        <div id="suggestions">
            <table>
                <tbody>
                    <tr>
                        <td><a href="search_browse?suggest=a">Aa</a></td>
                        <td><a href="search_browse?suggest=b">Bb</a></td>
                        <td><a href="search_browse?suggest=c">Cc</a></td>
                        <td><a href="search_browse?suggest=d">Dd</a></td>
                        <td><a href="search_browse?suggest=e">Ee</a></td>
                    </tr>
                    <tr>
                        <td><a href="search_browse?suggest=f">Ff</a></td>
                        <td><a href="search_browse?suggest=g">Gg</a></td>
                        <td><a href="search_browse?suggest=h">Hh</a></td>
                        <td><a href="search_browse?suggest=i">Ii</a></td>
                        <td><a href="search_browse?suggest=j">Jj</a></td>
                    </tr>
                    <tr>
                        <td><a href="search_browse?suggest=k">Kk</a></td>
                        <td><a href="search_browse?suggest=l">Ll</a></td>
                        <td><a href="search_browse?suggest=m">Mm</a></td>
                        <td><a href="search_browse?suggest=n">Nn</a></td>
                        <td><a href="search_browse?suggest=o">Oo</a></td>
                    </tr>
                    <tr>
                        <td><a href="search_browse?suggest=p">Pp</a></td>
                        <td><a href="search_browse?suggest=q">Qq</a></td>
                        <td><a href="search_browse?suggest=r">Rr</a></td>
                        <td><a href="search_browse?suggest=s">Ss</a></td>
                        <td><a href="search_browse?suggest=t">Tt</a></td>
                    </tr>
                    <tr>
                        <td><a href="search_browse?suggest=u">Uu</a></td>
                        <td><a href="search_browse?suggest=v">Vv</a></td>
                        <td><a href="search_browse?suggest=w">Ww</a></td>
                        <td><a href="search_browse?suggest=x">Xx</a></td>
                        <td><a href="search_browse?suggest=y">Yy</a></td>
                    </tr>
                    <tr>
                        <td><a href="search_browse?suggest=z">Zz</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="suggest">
            <p>Authors on letter <em>${letter}</em>:</p>
            <c:forEach items="${authorSuggester}" var="author">
                <p><a href='search_browse?suggest=${letter}&author=${author}&title=&publisher='>&emsp;${author}</a></p>
            </c:forEach>
            <hr>
            <p>Titles on letter <em>${letter}</em>:</p>
            <c:forEach items="${titleSuggester}" var="title">
                <p><a href='search_browse?suggest=${letter}&author=&title=${title}&publisher='>${title}</a></p>
            </c:forEach>
            <hr>
            <p>Publisher on letter <em>${letter}</em>:</p>
            <c:forEach items="${publisherSuggester}" var="publisher">
                <p><a href='search_browse?suggest=${letter}&author=&title=&publisher=${publisher}'>${publisher}</a></p>
            </c:forEach>
        </div>
        <hr>
        <table id="amount_result">
            <c:forEach items="${amount_result}" var="entry">
                <tr>
                    <td><p>Search for this ${entry.key} has ${entry.value} matches in library.</p></td>
                </tr>
            </c:forEach>
        </table>
        <c:set var="i" scope="session" value="0"/>
		<div id="books">
			<c:forEach items="${books}" var="item">
			<c:set var="i" scope="session" value="${i+1}"/>
                <p style="color: #008000b8;">${item.title}<h4>Author: ${item.author}&emsp;Publisher: ${item.publisher}</h4></p>
                <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="leftcol">
                        <c:choose>
                            <c:when test="${item.coverImage == 'ul'}">
                                <img src="images/cover.jpg" alt="image"/>
                            </c:when>
                             <c:otherwise>
                                <img src="data:image/jpeg;base64, ${item.coverImage}" alt="image"/>
                             </c:otherwise>
                        </c:choose>
                    </td>
                    <td valign="top" class="rightcol">
                        <c:choose>
                            <c:when test="${item.annotation == 'ul'}">
                                <p>Annotation</p>
                            </c:when>
                            <c:otherwise>
                                <p>${item.annotation}</p>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </table>
                <a href='javascript:getFirstPage("${text}","${i}", "${item.author}", "${item.title}", "${item.publisher}");'>Read Book</a>
                <div id="content${i}" hidden>
                    <p id="page_Book${i}"></p>
                    <p id="number_Book${i}"></p>
                    <a href='javascript:previousPage("${text}", "${i}","${item.author}", "${item.title}", "${item.publisher}");'>Previous page</a>&emsp;
                    <a href='javascript:nextPage("${text}","${i}","${item.author}", "${item.title}", "${item.publisher}");'> Next page</a>
                </div>
                <hr>
            </c:forEach>
            <a href='javascript:nextBooks();'></a>
		</div>
</section>
</body>
</html>
